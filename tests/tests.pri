INCLUDEPATH += $$PWD/../src $$PWD/../src/hunks $$PWD/../src/patches $$PWD/../src/commands
DEPENDPATH  += $$INCLUDEPATH .
CONFIG += qtestlib

test.files =
test.path = .
test.depends = all
!isEmpty(DESTDIR): test.commands = cd ./$(DESTDIR) &&
macx:      test.commands += ./$(QMAKE_TARGET).app/Contents/MacOS/$(QMAKE_TARGET)
else:unix: test.commands += ./$(QMAKE_TARGET)
else:      test.commands += $(QMAKE_TARGET)
embedded:  test.commands += -qws
INSTALLS += test

QMAKE_EXTRA_TARGETS += test

