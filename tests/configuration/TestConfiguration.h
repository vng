#ifndef TESTCURSOR_H
#define TESTCURSOR_H

#include <QObject>
#include <QtTest/QtTest>

class TestConfiguration : public QObject {
    Q_OBJECT
public:
    TestConfiguration();

private slots:
    void testFindRepo();
    void testReadBranchesConfig();
    void init();

private:
    QDir m_originalDir;
};

#endif
