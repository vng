include (../tests.pri)

HEADERS += TestConfiguration.h \
    Configuration.h \
    Vng.h \
    GitRunner.h \
    Logger.h \
    Logger_p.h \
    TrackedBranch.h \
    RemoteRepo.h \
    hunks/Hunk.h \
    hunks/ChangeSet.h \
    hunks/File.h \
    patches/Commit.h \
    patches/Branch.h \


SOURCES += TestConfiguration.cpp \
    Configuration.cpp \
    Vng.cpp \
    GitRunner.cpp \
    Logger.cpp \
    TrackedBranch.cpp \
    RemoteRepo.cpp \
    hunks/Hunk.cpp \
    hunks/ChangeSet.cpp \
    hunks/File.cpp \
    patches/Commit.cpp \
    patches/Branch.cpp \

