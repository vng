#include "TestConfiguration.h"

#include "Configuration.h"

#include <QDir>
#include <QList>

TestConfiguration::TestConfiguration()
{
    m_originalDir = QDir::current();
}

void TestConfiguration::init()
{
    QDir::setCurrent(m_originalDir.absolutePath());
}

void TestConfiguration::testFindRepo()
{
    QVERIFY(QDir(".").mkpath(".git"));
    QVERIFY(QDir(".").mkpath("foo/bar/baz"));

    Configuration configuration("dummy");
    QCOMPARE(configuration.repository(), m_originalDir);
    QCOMPARE(configuration.repositoryMetaDir(), QDir(m_originalDir.absolutePath() + "/.git"));

    QDir::setCurrent("foo/bar/baz");
    Configuration configuration2("dummy");
    QCOMPARE(configuration2.repository(), m_originalDir);
    QCOMPARE(configuration2.repositoryMetaDir(), QDir(m_originalDir.absolutePath() + "/.git"));
}

void TestConfiguration::testReadBranchesConfig()
{
    QVERIFY(QDir(".").mkpath(".git"));
    QFile config(".git/config");
    QVERIFY(config.open(QIODevice::WriteOnly));
    config.write("#comment\n"
        "[core]\n\trepositoryformatversion = 0\n\tfilemode = true\n\tbare = false\n[remote \"origin\"]\n"
        "\turl = server:sources/vng.git\n"
        "\tfetch = +refs/heads/*:refs/remotes/origin/*\n[branch \"master2\"]\n\tremote = origin\n"
        "\tmerge = refs/heads/master\n"
        "[vng \"remote\"]\n\tdefault = server:sources/vng.git\n\turl = server2:foo/bar.git\n"
        "[branch \"addCommand\"]\n\tremote = origin\n\tmerge = refs/heads/addCommand\n");
    config.close();

    Configuration configuration("dummy");
    QList<RemoteRepo> remotes = configuration.remotes();
    QCOMPARE(remotes.count(), 2);
    RemoteRepo remote = remotes.first();
    QCOMPARE(remote.name(), QString("origin"));
    QCOMPARE(remote.url(), QString("server:sources/vng.git"));
    QCOMPARE(remote.isDefault(), true);
    RemoteRepo repo2 = remotes.at(1);
    QCOMPARE(repo2.name(), QString("server2:foo/bar.git"));
    QCOMPARE(repo2.url(), QString("server2:foo/bar.git"));
    QCOMPARE(repo2.isDefault(), false);

    QList<TrackedBranch> branches = configuration.trackedBranches();
    QCOMPARE(branches.count(), 2);
    TrackedBranch master = branches.takeFirst();
    if (master.localName() != "master") { // swap
        branches.append(master);
        master = branches.takeFirst();
    }

    QCOMPARE(master.localName() , QString("master"));
    QCOMPARE(master.remoteName() , QString("master2"));
    QCOMPARE(master.remote().name(), remote.name());

    TrackedBranch addCommand = branches.takeFirst();
    QCOMPARE(addCommand.localName() , QString("addCommand"));
    QCOMPARE(addCommand.remoteName() , QString("addCommand"));
    QCOMPARE(addCommand.remote().name(), remote.name());
}

QTEST_MAIN(TestConfiguration)
