include (../tests.pri)

HEADERS += TestReadCommit.h \
    patches/Commit.h \
    patches/Branch.h \
    GitRunner.h \
    Logger.h \
    Logger_p.h \
    Configuration.h \
    TrackedBranch.h \
    RemoteRepo.h \
    Vng.h \
    hunks/ChangeSet.h \
    hunks/File.h \
    hunks/Hunk.h \


SOURCES += TestReadCommit.cpp \
    patches/Commit.cpp \
    patches/Branch.cpp \
    GitRunner.cpp \
    Logger.cpp \
    Configuration.cpp \
    TrackedBranch.cpp \
    RemoteRepo.cpp \
    Vng.cpp \
    hunks/ChangeSet.cpp \
    hunks/File.cpp \
    hunks/Hunk.cpp \

