#ifndef TESTPARSEDIFF_H
#define TESTPARSEDIFF_H

#include <QObject>
#include <QtTest/QtTest>

class TestParseDiff : public QObject {
    Q_OBJECT
public:
    TestParseDiff() {}

private slots:
    void testBasis();
};

#endif
