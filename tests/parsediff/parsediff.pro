include (../tests.pri)

HEADERS += TestParseDiff.h \
    patches/Commit.h \
    patches/Branch.h \
    hunks/ChangeSet.h \
    hunks/File.h \
    hunks/Hunk.h \
    GitRunner.h \
    Vng.h \
    Logger.h \
    Logger_p.h \
    Configuration.h \
    TrackedBranch.h \
    RemoteRepo.h \


SOURCES += TestParseDiff.cpp \
    patches/Commit.cpp \
    patches/Branch.cpp \
    hunks/ChangeSet.cpp \
    hunks/File.cpp \
    hunks/Hunk.cpp \
    Vng.cpp \
    GitRunner.cpp \
    Logger.cpp \
    Configuration.cpp \
    TrackedBranch.cpp \
    RemoteRepo.cpp \

