#include "TestParseDiff.h"

#include "hunks/ChangeSet.h"

void TestParseDiff::testBasis()
{
    const char *diff = "diff --git a/FOO b/FOO\n"
        "new file mode 100644\n"
        "index 0000000..94a9ed0\n"
        "--- /dev/null\n"
        "+++ b/FOO\n"
        "@@ -0,0 +1,674 @@\n"
        "+file content\n"
        "diff --git a/BAR b/BAR\n"
        "deleted file mode 100644\n"
        "index 94a9ed0..0000000\n"
        "--- a/BAR\n"
        "+++ /dev/null\n"
        "@@ -1,674 +0,0 @@\n"
        "-file content2\n"
        " \n"
        "diff --git a/todo b/todo\n"
        "index a4c7321..a0417af 100644\n"
        "--- a/todo\n"
        "+++ b/todo\n"
        "@@ -4,8 +4,6 @@ Commands still missing completely;\n"
        "       setpref       Set a value for a preference (test, predist, ...).\n"
        "       rollback      Record an inverse patch without changing the working directory.\n"
        "       annotate      Display which patch last modified something.\n"
        "-      trackdown     Locate the most recent version lacking an error.\n"
        "-      query         Query information which is stored by vng.\n"
        "       unpull        Opposite of pull; unsafe if patch is not in remote repository.\n"
        "       obliterate    Delete selected patches from the repository. (UNSAFE!)\n"
        "       send          Send by email a bundle of one or more patches.\n"
        "--- a/README\n"
        "+++ b/README\n"
        "@@ -6,8 +6,6 @@ Compiling\n"
        " To compile this project you need Qt4.4 or later, only the qt-core library is used.\n"
        " \n"
        " To generate a makefile call the 'qmake' command in the vng directory.  Make\n"
        "-sure you call the one from the right Qt release.\n"
        "-Some linux distros ship this command names 'qmake-qt4'\n"
        " \n"
        " After calling qmake, compile the project like you would any other.  This is\n"
        " platform dependent and could be \"nmake\", \"make\" etc.\n"
        " \n";

    QByteArray bytes(diff);
    QBuffer buffer(&bytes, 0);
    buffer.open(QBuffer::ReadOnly);
    QList<File> answer = ChangeSet::readGitDiff(buffer);
    QCOMPARE(answer.count(), 3);

    File file1 = answer[0];
    QCOMPARE(file1.fileName(), QByteArray("todo"));
    QCOMPARE(file1.oldFileName(), QByteArray("todo"));
    QCOMPARE(file1.oldSha1(), QString("a4c7321"));
    QCOMPARE(file1.sha1(), QString("a0417af"));
    QCOMPARE(file1.protection(), QString("100644"));
    QCOMPARE(file1.hunks().count(), 1);

    File file2 = answer[1];
    QCOMPARE(file2.oldFileName(), QByteArray("README"));
    QCOMPARE(file2.fileName(), QByteArray("README"));
    QCOMPARE(file2.sha1(), QString());
    QCOMPARE(file2.oldSha1(), QString());
    QCOMPARE(file2.protection(), QString());
    QCOMPARE(file2.hunks().count(), 1);

    File renamed = answer.last(); // renamed files go at the end..
    QCOMPARE(renamed.oldFileName(), QByteArray("BAR"));
    QCOMPARE(renamed.fileName(), QByteArray("FOO"));
    QCOMPARE(renamed.sha1(), QString("94a9ed0"));
    QCOMPARE(renamed.oldSha1(), QString("94a9ed0"));
    QCOMPARE(renamed.protection(), QString("100644"));
    QCOMPARE(renamed.hunks().count(), 1);
}

QTEST_MAIN(TestParseDiff)
