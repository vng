#include "RemoteRepo.h"

RemoteRepo::RemoteRepo()
{
}

RemoteRepo::RemoteRepo(const RemoteRepo &other)
    : d(other.d)
{
}

RemoteRepo::RemoteRepo(const QString &name, const QString &url)
    : d(new RemoteRepoPrivate())
{
    d->name = name;
    d->url = url;
}

RemoteRepo::~RemoteRepo()
{
}

void RemoteRepo::setIsDefault(bool on)
{
    d->isDefaultRepo = on;
}

bool RemoteRepo::isDefault() const
{
    return d->isDefaultRepo;
}

QString RemoteRepo::name() const
{
    return d->name;
}

QString RemoteRepo::url() const
{
    return d->url;
}
