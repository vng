/*
 * This file is part of the vng project
 * Copyright (C) 2008 Thomas Zander <tzander@trolltech.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Push.h"
#include "CommandLineParser.h"
#include "Logger.h"
#include "GitRunner.h"
#include "GenericCursor.h"
#include "Interview.h"
#include "Vng.h"

#include <QProcess>
#include <QDebug>
#include <QRegExp>

static const CommandLineOption options[] = {
    // TODO :)
    // {"--matches=PATTERN", "select patches matching PATTERN"},
    // {"-p REGEXP, --patches=REGEXP", "select patches matching REGEXP"},
    // {"-t REGEXP, --tags=REGEXP", "select tags matching REGEXP"},
    // {"-a, --all", "answer yes to all patches"},
    // {"-i, --interactive", "prompt user interactively"},
    // {"-s, --summary", "summarize changes"},
    // {"--no-summary", "don't summarize changes"},
    {"--set-default", "set default repository [DEFAULT]"},
    {"--no-set-default", "don't set default repository"},
    CommandLineLastOption
};

Push::Push()
    : AbstractCommand("push")
{
    CommandLineParser::addOptionDefinitions(options);
    CommandLineParser::setArgumentDefinition("push [Repository]" );
}

AbstractCommand::ReturnCodes Push::run()
{
    if (! checkInRepository())
        return NotInRepo;
    CommandLineParser *args = CommandLineParser::instance();


    // Find out which remote repo to use
    RemoteRepo remoteRepo;
    if (args->arguments().count() > 1) {
        remoteRepo = RemoteRepo(args->arguments().at(1), args->arguments().at(1));
    } else {
        foreach (const RemoteRepo &repo, m_config.remotes()) {
            if (repo.isDefault()) {
                remoteRepo = repo;
                break;
            }
        }
    }
    if (! remoteRepo.isValid()) {
        if (m_config.remotes().isEmpty()) {
            Logger::error() << "Vng failed: Please specify the remote repository you want to push to\n";
            return InvalidOptions;
        }
        if (m_config.remotes().size() > 1) {
            GenericCursor cursor;
            foreach (const RemoteRepo &repo, m_config.remotes()) {
                if (repo.url() == repo.name())
                    cursor.addDataItem(QLatin1String("   at ") + repo.url());
                else
                    cursor.addDataItem(QLatin1String("   '") + repo.name()
                            + QLatin1String("` (") + repo.url() + QLatin1Char(')'));
            }
            Interview interview(cursor, QLatin1String("Shall I use this repository?"));
            interview.setUsePager(shouldUsePager());
            if (!interview.start())
                return Ok;
            Q_ASSERT(!cursor.selectedItems().isEmpty());
            remoteRepo = m_config.remotes().at(cursor.selectedItems().first());
        } else {
            remoteRepo = m_config.remotes().first();
        }
    }
    Q_ASSERT(remoteRepo.isValid());

    Logger::warn() << "Pushing to `" << remoteRepo.url() << "'\n";
    Logger::warn().flush(); // make sure its printed before git asks for an ssh pwd.
    if (dryRun())
        return Ok;

    // TODO when not using --all ask the remote for all the refs it has and detect which ones we still have to push
    // TODO use interview to ask which refs to push instead of all below
    QProcess git;
    QStringList arguments;
    arguments << QLatin1String("push") << remoteRepo.url();
    GitRunner runner(git, arguments);
    AbstractCommand::ReturnCodes rc = runner.start(GitRunner::WaitForStandardOutput);
    if (rc != Ok) {
        Logger::error() << "Git push failed\n";
        return rc;
    }

    char buf[1024];
    while(true) { // just pipe out data
        git.waitForReadyRead(-1);
        qint64 readLength = git.read(buf, sizeof(buf)-1);
        if (readLength <= 0)
            break;
        buf[readLength] = 0;
        Logger::standardOut() << buf;
    }
    git.waitForFinished(-1);

    const bool makeDefault = args->contains(QLatin1String("set-default"))
        || (m_config.contains(QLatin1String("set-default"))
                && !args->contains(QLatin1String("no-set-default")));
    m_config.addRepo(remoteRepo, makeDefault ? Configuration::AddAsDefault
            : Configuration::AddNotDefault);
    return Ok;
}

QString Push::argumentDescription() const
{
    return QLatin1String("[REPOSITORY]");
}

QString Push::commandDescription() const
{
    return QLatin1String("push is the opposite of pull. push allows you to copy changes from the\n"
    "current repository into another repository.\n");
}
