/*
 * This file is part of the vng project
 * Copyright (C) 2009 Thomas Zander <tzander@trolltech.com>
 * Copyright (C) 2002-2004 David Roundy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Diff.h"
#include "../CommandLineParser.h"
#include "../Logger.h"
#include "../GitRunner.h"
#include "../hunks/ChangeSet.h"
#include "WhatsNew.h"
#include "Changes.h"

#include <QDebug>

static const CommandLineOption options[] = {
    {"--to-match PATTERN", "select changes up to a patch matching PATTERN"},
    {"--to-patch REGEXP", "select changes up to a patch matching REGEXP"},
    // {"--to-tag REGEXP", "select changes up to a tag matching REGEXP"},
    {"--from-match PATTERN", "select changes starting with a patch matching PATTERN"},
    {"--from-patch REGEXP", "select changes starting with a patch matching REGEXP"},
    // {"--from-tag REGEXP", "select changes starting with a tag matching REGEXP"},
    {"-n, --last NUMBER", "select the last NUMBER patches"},
    {"--match PATTERN", "select patches matching PATTERN"},
    {"-p, --patches REGEXP", "select patches matching REGEXP"},
    // {"-t, --tags=REGEXP", "select tags matching REGEXP"},
    {"--reverse", "show changes in reverse order"},
    {"--no-reverse", "don't show changes in reverse order"},
    CommandLineLastOption
};

Diff::Diff()
    : AbstractCommand("diff")
{
    CommandLineParser::addOptionDefinitions(options);
    CommandLineParser::setArgumentDefinition("diff [FILE or DIRECTORY]" );
}

AbstractCommand::ReturnCodes Diff::run()
{
    if (! checkInRepository())
        return NotInRepo;
    CommandLineParser *args = CommandLineParser::instance();

    QStringList arguments;
    arguments << QLatin1String("diff-tree") << QLatin1String("-p");

    bool parsedArguments = false;
    if (args->arguments().count() > 1) {
        foreach (Branch branch, m_config.allBranches()) { // check if our argument is a named branch.
            QString branchName = branch.branchName();
            if (branchName.endsWith(QLatin1String("/HEAD")))
                continue;
            bool first = true;
            int index = -1;
            foreach (QString arg, args->arguments()) {
                index++;
                if (first) {
                    first = false; // skip command, args for this command are next.
                    continue;
                }
                if (branchName == arg || (branchName.endsWith(arg) && branchName[branchName.length() - arg.length() - 1].unicode() == '/')) {
                    // its a named branch!
                    arguments << branch.commitTreeIsmSha1() << QLatin1String("HEAD");
                    parsedArguments = true;
                    break;
                }
            }
            if (parsedArguments)
                break;
        }

        if (!parsedArguments) { // check if our arguments are files on the filesystem.
            QStringList files;
            QStringList revisions;

            QString currentDir = QDir::current().absolutePath();
            moveToRoot(CheckFileSystem);
            foreach (QString arg, rebasedArguments()) {
                QFile file (arg);
                if (file.exists()) {
                    files << arg;
                } else {
                    revisions << arg;
                }
            }
            QDir::setCurrent(currentDir);
            if (revisions.count() > 2) {
                Logger::error() << "Vng Failed: unknown files or revisions passed";
                return InvalidOptions;
            }
//qDebug() << "  revisions" << revisions << "files" << files;
            // TODO if files have been passed we can't handle it with the diff-tree, so this will fail

            // split our arguments in files and revisions
            if (!revisions.isEmpty()) {
                arguments << revisions;
                if (revisions.count() == 1)
                    arguments << QLatin1String("HEAD");
                if (! files.isEmpty())
                    arguments << QLatin1String("--");
            }
            arguments << files;
            parsedArguments = true;
        }
    }

    const QStringList options = args->options();
    // always use 'whatsnew' unless the user passes a patch options
    if (!parsedArguments && !(options.contains(QLatin1String("to-match"))
        || options.contains(QLatin1String("to-patch"))
        || options.contains(QLatin1String("from-patch"))
        || options.contains(QLatin1String("from-match"))
        || options.contains(QLatin1String("last"))
        || options.contains(QLatin1String("match")))) {
        WhatsNew wn;
        wn.pullConfigDataFrom(this);
        return wn.printWhatsNew(true, false);
    }

    if (shouldUsePager())
        Logger::startPager();

    if (!parsedArguments) {
        QString revisions;
        Changes changes;
        changes.pullConfigDataFrom(this);
        ReturnCodes rc = changes.printChangeList(true, revisions);
        if (rc != Ok)
            return rc;
        QStringList revs = revisions.split(QLatin1Char(' '));
        if (revs.count() == 2) {
            arguments << revs.at(1);
            arguments << revs.at(0);
        }
    }

    QProcess git;
    GitRunner runner(git, arguments);
    ReturnCodes rc = runner.start(GitRunner::WaitForStandardOutput, GitRunner::FailureAccepted);
    if (rc == AbstractCommand::GitFailed) { // wrong args, most likely
        Logger::error() << "Vng failed: failed to understand arguments\n";
        return InvalidOptions;
    }
    if (rc) {
        Logger::error() << "Vng failed: Unknown reason\n";
        return rc;
    }

    QTextStream &out = Logger::standardOut();
    foreach (File file, ChangeSet::readGitDiff(git)) {
        file.outputWhatsChanged(out, m_config, false, true);
    }
    Logger::stopPager();

    return Ok;
}

QString Diff::argumentDescription() const
{
    return QLatin1String("[FILE or DIRECTORY]");
}

QString Diff::commandDescription() const
{
    return QLatin1String("Diff can be used to create a diff between two versions which are in your\n"
        "repository.  Specifying just --from-patch will get you a diff against\n"
        "your working copy.  If you give diff no version arguments, it gives\n"
        "you the same information as whatsnew except that the patch is\n"
        "formatted as the output of a diff command\n");
}
