/*
 * This file is part of the vng project
 * Copyright (C) 2008 Thomas Zander <tzander@trolltech.com>
 * Copyright (C) 2002-2004 David Roundy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Changes.h"
#include "CommandLineParser.h"
#include "GitRunner.h"
#include "Logger.h"
#include "Vng.h"
#include "hunks/ChangeSet.h"
#include "patches/Commit.h"
#include "patches/CommitsMatcher.h"

#include <QDebug>

class CommitIterator {
public:
    enum Direction {
        Forward,
        Reverse
    };

    CommitIterator(QProcess &input, Direction direction, int maxCommitCount);

    bool hasNext();
    Commit next();
    Commit current();

private:
    Commit getNext();

    Direction m_direction;
    QList<Commit> m_commitCache;
    QProcess &m_process;
    Commit m_current;
    CommitsMatcher m_matcher;
    int m_maxCommitCount;
};

CommitIterator::CommitIterator(QProcess &input, Direction direction, int maxCommitCount)
    : m_direction(direction),
    m_process(input),
    m_maxCommitCount(maxCommitCount)
{
    if (m_direction == Forward) {
        m_current = getNext();
    } else {
        if (maxCommitCount == -1)
            maxCommitCount = 1000; // caching more sounds bad
        while (m_commitCache.count () < maxCommitCount) {
            Commit commit = getNext();
            if (commit.isValid())
                m_commitCache << commit;
            else
                break;
        }
        m_process.terminate();
        if (!m_commitCache.isEmpty())
            m_current = m_commitCache.takeLast();
    }
}

Commit CommitIterator::next()
{
    if (m_direction == Forward) {
        if (m_current.isValid())
            m_current = getNext();
        else
            m_current = Commit();
    } else {
        if (!m_commitCache.isEmpty())
            m_current = m_commitCache.takeLast();
        else
            m_current = Commit();
    }
    return m_current;
}

Commit CommitIterator::current()
{
    return m_current;
}

Commit CommitIterator::getNext()
{
    if (m_maxCommitCount == 0) {
        m_process.terminate();
        return Commit();
    }
    while (true) {
        Commit commit = Commit::createFromStream(&m_process);
        if (! commit.isValid())
            return commit;
        switch(m_matcher.match(commit)) {
        case CommitsMatcher::SkipPatch:
            continue;
        case CommitsMatcher::ShowPatch:
            --m_maxCommitCount;
            return commit;
        case CommitsMatcher::Exit:
            m_process.terminate();
            return Commit();
        }
    }
}


static const CommandLineOption options[] = {
    // {"-a, --all", "answer yes to all patches"},
    {"--to-match PATTERN", "select changes up to a patch matching PATTERN"},
    {"--to-patch REGEXP", "select changes up to a patch matching REGEXP"},
    // {"--to-tag REGEXP", "select changes up to a tag matching REGEXP"},
    {"--from-match PATTERN", "select changes starting with a patch matching PATTERN"},
    {"--from-patch REGEXP", "select changes starting with a patch matching REGEXP"},
    // {"--from-tag REGEXP", "select changes starting with a tag matching REGEXP"},
    {"-n, --last NUMBER", "select the last NUMBER patches"},
    {"--match PATTERN", "select patches matching PATTERN"},
    {"-p, --patches REGEXP", "select patches matching REGEXP"},
    // {"-t, --tags=REGEXP", "select tags matching REGEXP"},
    // {"--context", "give output suitable for get --context"},
    // {"--xml-output", "generate XML formatted output"},
    // {"--human-readable", "give human-readable output"},
    {"-s, --summary", "summarize changes"},
    {"--no-summary", "don't summarize changes"},
    {"--reverse", "show changes in reverse order"},
    {"--no-reverse", "don't show changes in reverse order"},
    CommandLineLastOption
};

Changes::Changes()
    : AbstractCommand("changes")
{
    CommandLineParser::addOptionDefinitions(options);
    CommandLineParser::setArgumentDefinition("changes [FILE or DIRECTORY]" );
}

QString Changes::argumentDescription() const
{
    return QLatin1String("[FILE or DIRECTORY]");
}

QString Changes::commandDescription() const
{
    return QLatin1String("Changes gives a changelog-style summary of the repository history.\n");
}

AbstractCommand::ReturnCodes Changes::run()
{
    if (! checkInRepository())
        return NotInRepo;
    QString revisions; // ignored
    return printChangeList(false, revisions);
}

AbstractCommand::ReturnCodes Changes::printChangeList(bool tight, QString &revisions)
{
    CommandLineParser *args = CommandLineParser::instance();

    QProcess git;
    QStringList arguments;
    arguments << QLatin1String("whatchanged") << QLatin1String("--no-abbrev") << QLatin1String("--pretty=raw");

    int maxPatches = -1;
    if (args->contains(QLatin1String("last"))) {
        QString last = args->optionArgument(QLatin1String("last"));
        bool ok;
        maxPatches = last.toInt(&ok);
        if (!ok) {
            Logger::warn() << "\nFailed parsing your 'last' parameter. Reading your mind failed too, so I'll just use 1\n";
            Logger::warn().flush();
            maxPatches = 1;
        }
    }

    QList<int> usedArguments;
    usedArguments << 0;
    if (args->arguments().count() > 1) {
        foreach (Branch branch, m_config.allBranches()) {
            QString branchName = branch.branchName();
            if (branchName.endsWith(QLatin1String("/HEAD")))
                continue;
            bool first = true;
            int index = -1;
            foreach (QString arg, args->arguments()) {
                index++;
                if (first) {
                    first = false; // skip command, args for this command are next.
                    continue;
                }
                if (branchName == arg || (branchName.endsWith(arg) && branchName[branchName.length() - arg.length() - 1].unicode() == '/')) {
                    arguments << branchName;
                    usedArguments << index;
                    break;
                }
            }
        }
    }

    // now we have to use the rest of the arguments the user passed.
    int index = 0;
    QStringList unknownArguments;
    foreach (QString arg, args->arguments()) {
        if (! usedArguments.contains(index++)) {
            arguments << arg;
            unknownArguments << arg;
        }
    }

    GitRunner runner(git, arguments);
    runner.setTimeout(-1);
    ReturnCodes rc = runner.start(GitRunner::WaitForStandardOutput);
    if (rc) {
        Logger::error() << "Vng failed: Unknown branch or revision: ";
        foreach(QString arg, unknownArguments)
            Logger::error() <<  "`" << arg << "' ";;
        Logger::error() <<  endl;
        return rc;
    }
    if (!tight && shouldUsePager())
        Logger::startPager();

    const bool showSummery = (m_config.contains(QLatin1String("summary")) && !args->contains(QLatin1String("no-summary")))
        || args->contains(QLatin1String("summary"));
    QTextStream &out = Logger::standardOut();
    CommitsMatcher matcher;
    QString firstCommit, lastCommit;

    const bool reverse = (m_config.contains(QLatin1String("reverse")) && !args->contains(QLatin1String("no-reverse")))
        || args->contains(QLatin1String("reverse"));
    CommitIterator iter(git, reverse ? CommitIterator::Reverse : CommitIterator::Forward, maxPatches);
    while (true) {
        Commit commit = iter.current();
        if (! commit.isValid())
            break;
        if (firstCommit.isEmpty())
            firstCommit = commit.commitTreeIsmSha1();
        lastCommit = commit.commitTreeIsmSha1() + QLatin1Char('^');

        out << commit.commitTime().toString() << QLatin1Char(' ');
        m_config.colorize(out);
        out << commit.author();
        m_config.normalColor(out);
        out << endl;
        if (commit.author() != commit.committer()) {
            out << " By ";
            m_config.colorize(out);
            out << commit.committer();
            m_config.normalColor(out);
            out << endl;
        }
        out << " ID " << commit.commitTreeIsm() << endl;
        out << commit.logMessage();
        if (!tight && (Logger::verbosity() >= Logger::Verbose || showSummery)) {
            ChangeSet cs = commit.changeSet();
            cs.generateHunks();
            if (cs.count())
                out << endl;
            for (int i=0; i < cs.count(); ++i) {
                File file = cs.file(i);
                if (file.oldFileName().isEmpty())
                    out <<"  A " << QString::fromUtf8(file.fileName());
                else if (file.fileName().isEmpty())
                    out <<"  D " << QString::fromUtf8(file.oldFileName());
                else
                    out <<"  M " << QString::fromUtf8(file.fileName());
                if (Logger::verbosity() < Logger::Verbose) {
                    if (file.linesRemoved() > 0)
                        out << " -" << file.linesRemoved();
                    if (file.linesAdded() > 0)
                        out << " +" << file.linesAdded();
                }
                out << endl;
                if (Logger::verbosity() >= Logger::Verbose)
                    file.outputWhatsChanged(out, m_config, false, false);
            }
        }
        if (!tight)
            out << endl;
        Logger::flushPager();
        if (! out.device()->isWritable()) { // output cancelled; lets kill git.
            git.kill();
            break;
        }
        iter.next();
    }
    if (!tight)
        Logger::stopPager();
    git.waitForFinished();
    revisions = firstCommit +QLatin1Char(' ')+ lastCommit;
    return Ok;
}
