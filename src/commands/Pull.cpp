/*
 * This file is part of the vng project
 * Copyright (C) 2008-2009 Thomas Zander <tzander@trolltech.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Pull.h"
#include "CommandLineParser.h"
#include "Logger.h"
#include "GitRunner.h"
#include "GenericCursor.h"
#include "Interview.h"
#include "Vng.h"

#include <QProcess>
#include <QDebug>
#include <QList>
#include <QRegExp>

static const CommandLineOption options[] = {
    // TODO :)
    // {"--matches=PATTERN", "select patches matching PATTERN"},
    // {"-p REGEXP, --patches=REGEXP", "select patches matching REGEXP"},
    // {"-t REGEXP, --tags=REGEXP", "select tags matching REGEXP"},
    // {"-a, --all", "answer yes to all patches"},
    // {"-i, --interactive", "prompt user interactively"},
    // {"-s, --summary", "summarize changes"},
    // {"--no-summary", "don't summarize changes"},
    {"--set-default", "set default repository [DEFAULT]"},
    {"--no-set-default", "don't set default repository"},
    CommandLineLastOption
};

Pull::Pull()
    : AbstractCommand("pull")
{
    CommandLineParser::addOptionDefinitions(options);
    CommandLineParser::setArgumentDefinition("pull [Repository]" );
}

static QString fetchSha1FromRef(QString ref) // inline again?
{
    QFile localRef(ref);
    char buf[50];
    if (localRef.open(QIODevice::ReadOnly)) {
        qint64 lineLength = Vng::readLine(&localRef, buf, sizeof(buf));
        if (lineLength >= 40)
            return QString::fromLatin1(buf, 40);
    }
    return QString();
}

struct RemoteBranch {
    QString sha1;
    QString ref;
    TrackedBranch localBranch;
};

AbstractCommand::ReturnCodes Pull::run()
{
    if (! checkInRepository())
        return NotInRepo;
    CommandLineParser *args = CommandLineParser::instance();

    // Find out which remote repo to use
    RemoteRepo remoteRepo;
    if (args->arguments().count() > 1) {
        remoteRepo = RemoteRepo(args->arguments().at(1), args->arguments().at(1));
    } else {
        foreach (const RemoteRepo &repo, m_config.remotes()) {
            if (repo.isDefault()) {
                remoteRepo = repo;
                break;
            }
        }
    }
    if (! remoteRepo.isValid()) {
        if (m_config.remotes().isEmpty()) {
            Logger::error() << "Vng failed: Please specify the remote repository you want to pull from\n";
            return InvalidOptions;
        }
        if (m_config.remotes().size() > 1) {
            GenericCursor cursor;
            foreach (const RemoteRepo &repo, m_config.remotes()) {
                if (repo.url() == repo.name())
                    cursor.addDataItem(QLatin1String("   at ") + repo.url());
                else
                    cursor.addDataItem(QLatin1String("   '") + repo.name()
                            + QLatin1String("` (") + repo.url() + QLatin1Char(')'));
            }
            Interview interview(cursor, QLatin1String("Shall I use this repository?"));
            interview.setUsePager(shouldUsePager());
            if (!interview.start())
                return Ok;
            Q_ASSERT(!cursor.selectedItems().isEmpty());
            remoteRepo = m_config.remotes().at(cursor.selectedItems().first());
        } else {
            remoteRepo = m_config.remotes().first();
        }
    }
    Q_ASSERT(remoteRepo.isValid());
    Logger::debug() << "pulling from " << remoteRepo.name() << endl;

    QProcess git;
    QStringList arguments;
    arguments << QLatin1String("ls-remote") << QLatin1String("--heads")
        << QLatin1String("--tags") << remoteRepo.url();
    GitRunner runner(git, arguments);
    runner.setTimeout(5 * 60 * 1000); // network stuff, 5 min timeout.
    ReturnCodes rc = runner.start(GitRunner::WaitForStandardOutput, GitRunner::FailureAccepted);
    if (rc == AbstractCommand::GitFailed) {
        if (git.exitCode() == 128) {
            QString error = QString::fromLocal8Bit(git.readAllStandardError());
            Logger::error() << "Vng failed: can not read the remote repo";
            if (error.startsWith(QLatin1String("ssh:"))) {
                Logger::error() << ", please check network or and access rights.";
            } else if (error.indexOf(QLatin1String("does not look like a v2 bundle file"))) {
                Logger::error() << ", it doesn't look like a valid repo.";
            }
            Logger::error() << "\n";
            Logger::info() << error;
            return rc;
        }
    }
    if (rc) {
        Logger::error() << "Vng failed: No repository found at `" << remoteRepo.url() << "'\n";
        return rc;
    }

    // this gets the exported branches on the remote.
    QList<RemoteBranch> remotes;
    char buf[120];
    while(true) {
        qint64 lineLength = Vng::readLine(&git, buf, sizeof(buf));
        if (lineLength == -1)
            break;
        if (lineLength <= 42)
            continue;
        RemoteBranch branch;
        branch.sha1 = QString::fromLatin1(buf, 40);
        branch.ref = QString::fromAscii(buf + 41, lineLength - 42);
        int index = branch.ref.lastIndexOf(QLatin1Char('/'));
        if (index >= 0)
            branch.ref = branch.ref.mid(index+1);
        remotes.append(branch);
    }

    QString remotesDir = m_config.repositoryMetaDir().absolutePath()
        + QLatin1String("/refs/remotes/");

    QList<RemoteBranch> matchingBranches;
    QList<RemoteBranch> newBranches;
    foreach (const RemoteBranch &remoteBranch, remotes) {
        bool foundMatch = false;
        foreach (const TrackedBranch &localBranch, m_config.trackedBranches()) {
            if (localBranch.remoteName() == remoteBranch.ref) {
                foundMatch = true;
                RemoteBranch branch = remoteBranch;
                branch.localBranch = localBranch;
                matchingBranches << branch;
                break;
            }
        }
        if (!foundMatch) {
            QString sha1 = fetchSha1FromRef(remotesDir
                + remoteRepo.name() + QLatin1Char('/') + remoteBranch.ref);
            if (remoteBranch.sha1 != sha1)
                newBranches << remoteBranch;
        }
    }

    QList<RemoteBranch> toFetchBranches;
    if (matchingBranches.count() > 1 || !newBranches.isEmpty()) {
        GenericCursor cursor(GenericCursor::ExitWhenDone);
        foreach (const RemoteBranch &repo, matchingBranches)
            // TODO mark current branch somehow
            cursor.addDataItem(QLatin1String("  branch: '") + repo.ref + QLatin1Char('`'));
        foreach (const RemoteBranch &repo, newBranches)
            cursor.addDataItem(QLatin1String("  new branch: '") + repo.ref + QLatin1Char('`'));
        Interview interview(cursor, QLatin1String("Update this branch?"));
        interview.setUsePager(shouldUsePager());
        if (!interview.start())
            return Ok;
        foreach (int selected, cursor.selectedItems()) {
            if (selected < matchingBranches.count()) {
                toFetchBranches << matchingBranches.at(selected);
            } else {
                toFetchBranches << newBranches.at(selected - matchingBranches.count());
            }
        }
    } else {
        toFetchBranches << matchingBranches;
    }

    if (dryRun())
        return Ok;

    if (toFetchBranches.isEmpty()) {
        Logger::warn() << "No branches selected, nothing to do." << endl;
        return Ok;
    }

    arguments.clear();
    arguments << QLatin1String("fetch-pack") << QLatin1String("--no-progress") << remoteRepo.url();
    foreach (const RemoteBranch &branch, toFetchBranches) {
        arguments << QLatin1String("refs/heads/") + branch.ref;
    }

    runner.setArguments(arguments);
    rc = runner.start(GitRunner::WaitUntilFinished);
    if (rc) {
        Logger::error() << "Vng failed: fetching remote data didn't work\n";
        return rc;
    }
    const bool makeDefault = args->contains(QLatin1String("set-default"))
        || (m_config.contains(QLatin1String("set-default"))
                && !args->contains(QLatin1String("no-set-default")));
    m_config.addRepo(remoteRepo, makeDefault ? Configuration::AddAsDefault
            : Configuration::AddNotDefault);

    // update and create remote refs
    QString remoteDir = remotesDir + remoteRepo.name() + QLatin1Char('/');
    foreach (const RemoteBranch &branch, toFetchBranches) {
        QFile ref(remoteDir + branch.ref);
        // lets open it first and compare since writing is much more expensive.
        if (ref.open(QIODevice::ReadOnly)) {
            qint64 lineLength = Vng::readLine(&ref, buf, sizeof(buf));
            if (lineLength >= 40) {
                QString currentContent = QString::fromLatin1(buf, 40);
                if (currentContent == branch.sha1)
                    continue;
            }
            ref.close();
        }

        if (ref.open(QIODevice::WriteOnly)) {
            ref.write(branch.sha1.toAscii());
            ref.close();
        }
    }

    // now we check the branch currently checked out.
    // We then check if its in the tracked branches and see what the remote name is
    // if the remote branch is changed we merge our checkout.
    Branch head;
    foreach (Branch branch, m_config.branches()) {
        if (branch.isHead()) {
            head = branch;
            break;
        }
    }
    if (!head.isValid()) {
        Logger::warn() << "Your workdir is not on any branch, it won't be changed\n";
        return Ok;
    }
    Commit newHead;
    foreach (const RemoteBranch &branch, matchingBranches) {
        if (branch.localBranch.isValid() && QLatin1String("heads/")
                + branch.localBranch.localName() == head.branchName()) {
            if (head.commitTreeIsmSha1() == branch.sha1) {
                Logger::warn() << "Already up-to-date.\n";
                return Ok;
            }
            newHead = Commit(branch.sha1);
            break;
        }
    }
    if (! newHead.isValid()) {
        Logger::warn() << "Your workdir is not using a tracked branch, it won't be changed\n";
        return Ok;
    }

    arguments.clear();
    arguments << QLatin1String("merge") << newHead.commitTreeIsmSha1();
    runner.setArguments(arguments);
    Logger::standardOut().flush();
    rc = runner.start(GitRunner::WaitForStandardOutput);

    while(true) { // just pipe out data
        git.waitForReadyRead(-1);
        qint64 readLength = git.read(buf, sizeof(buf)-1);
        if (readLength <= 0)
            break;
        buf[readLength] = 0;
        Logger::standardOut() << buf;
        Logger::standardOut().flush();
    }
    return rc;
}

QString Pull::argumentDescription() const
{
    return QLatin1String("[REPOSITORY]");
}

QString Pull::commandDescription() const
{
    return QLatin1String("Pull is used to bring changes made in another repository into the current\nn"
    "repository (that is, either the one in the current directory, or the one\n"
    "specified with the --repodir option). Pull allows you to bring over all or\n"
    "some of the branches that are in that repository but not in this one. Pull\n"
    "accepts arguments, which are URLs from which to pull, and when called\n"
    "without an argument, pull will use the repository from which you have most\n"
    "recently either pushed or pulled.\n");
}
