/*
 * This file is part of the vng project
 * Copyright (C) 2008-2009 Thomas Zander <tzander@trolltech.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ChangeSet.h"
#include "../GitRunner.h"
#include "../Logger.h" // for debugging level only
#include "../AbstractCommand.h"
#include "../Vng.h"

#include <QProcess>
#include <QThread>
#include <QMutexLocker>
#include <QMutex>
#include <QWaitCondition>
#include <QDebug>

class HunksFetcher : public QThread
{
public:
    HunksFetcher(const QList<File> &files, ChangeSet &changeSet, bool changeSetOnIndex)
        : m_files(files),
            m_changeSet(changeSet),
            m_changeSetOnIndex(changeSetOnIndex),
            m_interrupted(false)
    {
    }

    void run()
    {
        setPriority(QThread::LowPriority);
        foreach(File file, m_files) {
            if (m_interrupted)
                break;
            m_changeSet.lockFile(file);

            file.fetchHunks(m_changeSetOnIndex);

            if (!file.isBinary() && file.count() == 0 && !file.hasChanged()) { // No change in file at all.
                Logger::debug() << "file: `" << QString::fromUtf8(file.oldFileName()) << "' => `" << QString::fromUtf8(file.fileName()) << "'\n";
                Logger::debug() << "  +- Unchanged file, skipping\n";
                m_changeSet.removeFile(file);
                QProcess git;
                QStringList arguments;
                arguments << "update-index" << "-q" <<  "--refresh" << file.fileName();
                GitRunner runner(git, arguments);
                runner.start(GitRunner::WaitUntilFinished);
                continue;
            }
            if (file.fileName().isEmpty() || file.oldFileName().isEmpty())
                file.setProtectionAcceptance(Vng::Accepted);

            int i=0;
            if (Logger::verbosity() >= Logger::Debug) {
                Logger::debug() << "file: `" << QString::fromUtf8(file.oldFileName()) << "' => `" << QString::fromUtf8(file.fileName()) << "'\n";
                if (file.isBinary())
                    Logger::debug() << "  +-  is a binary file" << endl;
                Logger::debug() << "  +- " << file.oldProtection() << " => " << file.protection() << endl;
                foreach(Hunk h, file.hunks()) {
                    Logger::debug() << "  +-(" << i++ << ") @" << h.lineNumber() << "; " << h.patch().size() << " bytes\n";
                    for(int i = 0; i < h.subHunkCount(); i++) {
                        Logger::debug() << "       " << i <<"/"<< h.subHunkCount() <<"; "<< h.subHunk(i).size() <<" bytes\n";
                    }
                }
            }
        }
        m_changeSet.lockFile(File());
        m_changeSet.allHunksFetched();
    }

    void interrupt()
    {
        m_interrupted = true;
    }

private:
    QList<File> m_files;
    ChangeSet &m_changeSet;
    bool m_changeSetOnIndex, m_interrupted;
};

class ChangeSet::Private
{
public:
    Private() :
        changeSetOnIndex(false),
        hunksFetcher(0),
        ref(1),
        finishedOneHunk(true)
    {
    }

    ~Private() {
        if (hunksFetcher) {
            hunksFetcher->interrupt();
            hunksFetcher->wait();
            delete hunksFetcher;
        }
    }

    QList<File> files;
    bool changeSetOnIndex; // the changesSet shows the changes of the working dir
    QMutex fileAccessLock;
    QWaitCondition fileAccessWaiter;
    QWaitCondition cursorAccessWaiter;
    QMutex cursorAccessLock;
    File lockedFile;
    HunksFetcher *hunksFetcher;
#if QT_VERSION >= 0x040400
    QAtomicInt ref;
#else
    int ref;
#endif
    bool finishedOneHunk;
};

ChangeSet::ChangeSet()
    : d(new Private())
{
}

ChangeSet::~ChangeSet()
{
#if QT_VERSION >= 0x040400
    if (!d->ref.deref())
#else
    if (--d->ref == 0)
#endif
        delete d;
}

ChangeSet::ChangeSet(const ChangeSet &other)
    : d(other.d)
{
#if QT_VERSION >= 0x040400
    d->ref.ref();
#else
    d->ref++;
#endif
}

AbstractCommand::ReturnCodes ChangeSet::fillFromDiffFile(QIODevice &file)
{
    file.open(QIODevice::ReadOnly);

    foreach (File f, readGitDiff(file))
        addFile(f);

    if (Logger::verbosity() >= Logger::Debug) {
        foreach(File f, d->files) {
            Logger::debug() << "changes in file: " << f.fileName() << endl;
            int i=0;
            foreach(Hunk h, f.hunks()) {
                Logger::debug() << "  +-(" << i++ << ") @" << h.lineNumber() << "; " << h.patch().size() << " bytes\n";
                for(int i = 0; i < h.subHunkCount(); i++) {
                    Logger::debug() << "       " << i <<"/"<< h.subHunkCount() <<"; "<< h.subHunk(i).size() <<" bytes\n";
                }
            }
        }
        Logger::debug().flush();
    }
    return AbstractCommand::Ok;
}

AbstractCommand::ReturnCodes ChangeSet::fillFromCurrentChanges(const QStringList &paths, bool doGenerateHunks )
{
    d->changeSetOnIndex = true;
    QDir refs(".git/refs/heads");
    bool emptyRepo = refs.count() == 2; // only '.' and '..'
    if (emptyRepo) {
        QFile refs(QLatin1String(".git/packed-refs"));
        emptyRepo = !refs.exists();
    }
    if (emptyRepo) { // all files added are new, just add all.
        QProcess git;
        QStringList arguments;
        arguments << "ls-files" << "-s";
        GitRunner runner(git, arguments);
        AbstractCommand::ReturnCodes rc = runner.start(GitRunner::WaitForStandardOutput);
        if (rc)
            return rc;
        char buf[1024];
        while(true) {
            qint64 lineLength = Vng::readLine(&git, buf, sizeof(buf));
            if (lineLength == -1)
                break;
            File file;
            file.setProtection(QString::fromAscii(buf, 6));
            file.setSha1(QString::fromAscii(buf+7, 40));
            file.setFileName(File::escapeGitFilename(QByteArray(buf + 50, lineLength - 51)));
            d->files.append(file);
        }
        return AbstractCommand::Ok;
    }

    // TODO the below misses the usecase of vng add and then a filesystem rm. Use diff-index --cached to show those.
    QProcess git;
    QStringList arguments;
    arguments << "diff-index" << "-M" << "HEAD";
    if (! paths.isEmpty())
        arguments << "--" << paths;

    // for each line
    // new file:
    // :000000 100644 0000000000000000000000000000000000000000 8c3ae1d344f18b23c3bdde5d26658b70b03c65d9 A      bar
    // rename main.cpp => notmain.cpp
    // :100644 100644 e58cfe72cb7a9559a0090886bea5b0ce00db6b47 477c729e5abbd701eb708df563e7e4f749b50435 R074   main.cpp        notmain.cpp
    // normal change
    // :100644 100644 0bdd73e9ea0ba026f6796799946c4bfc9dd1b0b8 0000000000000000000000000000000000000000 M      test

    GitRunner runner(git, arguments);
    AbstractCommand::ReturnCodes rc = runner.start(GitRunner::WaitForStandardOutput);
    if (rc)
        return rc;
    char buf[1024];
    while(true) {
        qint64 lineLength = Vng::readLine(&git, buf, sizeof(buf));
        if (lineLength == -1)
            break;
        if (lineLength > 0 && buf[0] != ':') // not a diff line, ignore.
            continue;

        File file;
        file.setOldProtection(QString::fromAscii(buf+1, 6));
        file.setProtection(QString::fromAscii(buf+8, 6));
        file.setOldSha1(QString::fromAscii(buf+15, 40));
        file.setSha1(QString::fromAscii(buf+56, 40));
        int offset = 98;
        while (buf[offset] != '\t' && offset < lineLength)
            offset++;
        if (buf[97] == 'R') { // rename
            int tab = offset + 1;
            while (buf[tab] != '\t' && tab < lineLength)
                tab++;
            file.setOldFileName(File::escapeGitFilename(QByteArray(buf + offset + 1, tab - offset - 1)));
            file.setFileName(File::escapeGitFilename(QByteArray(buf + tab + 1, lineLength - tab - 2)));
        }
        else if (buf[97] == 'C') { // Copied file
            int tab = offset + 1;
            while (buf[tab] != '\t' && tab < lineLength)
                tab++;
            QByteArray filename(buf + offset + 1, tab - offset - 1);
            filename = File::escapeGitFilename(filename);
            file.setOldFileName(filename);
            file.setFileName(filename);
        }
        else {
            QByteArray filename(buf + offset + 1, lineLength - offset - 2);
            filename = File::escapeGitFilename(filename);
            if (buf[97] != 'A') // Add
                file.setOldFileName(filename);
            if (buf[97] != 'D') // Delete
                file.setFileName(filename);
        }
        d->files.append(file);
    }
    if (doGenerateHunks)
        generateHunks();

    // call git-diff-files which will find all files that have been removed from the filesystem but are in the index.
    // Since we don't like the index we just remove them from the index here.
    arguments.clear();
    arguments << "diff-files";
    if (! paths.isEmpty())
        arguments << "--" << paths;
    runner.setArguments(arguments);
    rc = runner.start(GitRunner::WaitForStandardOutput);
    if (rc)
        return rc;
    arguments.clear();
    arguments << "update-index" << "--remove";
    while(true) {
        qint64 lineLength = Vng::readLine(&git, buf, sizeof(buf));
        if (lineLength == -1)
            break;
        if (lineLength > 0 && buf[0] != ':') // not a diff line, ignore.
            continue;
        if (lineLength < 97 ||buf[97] != 'D')
            continue;

        int offset = 98;
        while (buf[offset] != '\t' && offset < lineLength)
            offset++;
        arguments.append(QString::fromUtf8(File::escapeGitFilename(QByteArray(buf + offset + 1, lineLength - offset - 2))));
    }
    if (arguments.count() > 2) {
        runner.setArguments(arguments);
        runner.start(GitRunner::WaitUntilFinished);
    }

    return AbstractCommand::Ok;
}

void ChangeSet::generateHunks()
{
    Q_ASSERT(d->hunksFetcher == 0);
    d->hunksFetcher = new HunksFetcher(d->files, *this, d->changeSetOnIndex);
    d->finishedOneHunk = false;
    d->hunksFetcher->start();
}

void ChangeSet::lockFile(const File &file)
{
    // qDebug() << "ChangeSet::lockFile";
    QMutexLocker ml(&d->fileAccessLock);
    d->lockedFile = file;
    if (d->files.count() == 0 || d->files.at(0) != file) { // as soon as we have done a file, we can start the interaction
        // qDebug() << "  unlock cursorAccessWaiter";
        d->cursorAccessLock.lock();
        d->finishedOneHunk = true;
        d->cursorAccessWaiter.wakeAll();
        d->cursorAccessLock.unlock();
    }

    d->fileAccessWaiter.wakeAll();
    // qDebug() << "~ChangeSet::lockFile";
}

void ChangeSet::removeFile(const File &file)
{
    QMutexLocker ml(&d->fileAccessLock);
    // TODO move the cursor if this file is the current file.
    d->files.removeAll(file);
    d->fileAccessWaiter.wakeAll();
}

void ChangeSet::allHunksFetched()
{
    // qDebug() << "ChangeSet::allHunksFetched";
    QMutexLocker ml(&d->fileAccessLock);
    d->lockedFile = File();
    d->fileAccessWaiter.wakeAll();
    // qDebug() << "~ChangeSet::allHunksFetched";
}

bool ChangeSet::hasAllHunks() const
{
    return d->hunksFetcher == 0 || d->hunksFetcher->isFinished();
}

// static
QList<File> ChangeSet::readGitDiff(QIODevice &git, File *fileToDiff)
{
    class States {
      public:
        States(QIODevice &device, File *fileToDiff)
            : m_input(device),
            m_fileToDiff(fileToDiff),
            m_state(Empty)
        {
        }

        void start() {
            Q_ASSERT(m_filesInDiff.isEmpty()); // only call start once, please ;)
            m_state = Empty;
            while(true) {
                qint64 lineLength = Vng::readLine(&m_input, buf, sizeof(buf));
                if (lineLength == -1 || m_state == Empty)
                    storeFile();

                if (lineLength == -1)
                    break;
                if (addLine(QString::fromLocal8Bit(buf, lineLength)))
                    break;
                if (m_state == InPatch) {
                    QByteArray array(buf, lineLength);
                    m_hunk.addLine(array);
                }
            }
            m_input.close();

            // try to find out if there are renames
            foreach (File addedFile, m_newFiles) {
                foreach (File removedFile, m_removedFiles) {
                    if (!addedFile.sha1().isEmpty() && removedFile.oldSha1() == addedFile.sha1()) {
                        // TODO if this is a partial sha1 we may want to check some of the content
                        m_removedFiles.removeAll(removedFile);
                        addedFile.setOldSha1(removedFile.oldSha1());
                        addedFile.setOldFileName(removedFile.oldFileName());
                        addedFile.setOldProtection(removedFile.oldProtection());
                        break;
                    }
                }
            }
            m_filesInDiff << m_newFiles;
            m_filesInDiff << m_removedFiles;
            m_newFiles.clear();
            m_removedFiles.clear();
        }

        QList<File> results() const {
            return m_filesInDiff;
        }
      private:
        enum State {
            InPatch,
            Empty,
            InHeader
        };

        void storeFile() {
            m_file.addHunk(m_hunk);
            m_hunk = Hunk();
            if (m_file.isValid()) {
                if (m_file.oldFileName().isEmpty())
                    m_newFiles << m_file;
                else if (m_file.fileName().isEmpty())
                    m_removedFiles << m_file;
                else
                    m_filesInDiff << m_file;
            }
            if (m_fileToDiff)
                m_file = File(*m_fileToDiff);
            else
                m_file = File();
            m_state = Empty;
        }

        // returns true if we should exit.
        bool addLine(const QString &line) {
            const bool newfile = line.startsWith("--- /dev/null");
            if (line.length() > 6 && (newfile || line.startsWith("--- a/")
                || line.startsWith("--- \"a/"))) {
                if (m_state == InPatch)
                    storeFile();
                m_state = InHeader;
                if (!newfile && m_fileToDiff == 0) {
                    if (line[4].unicode() == '"') { // git-encoding...
                        QByteArray array(buf + 7, strlen(buf) - 8);
                        array.prepend('"');
                        m_file.setOldFileName(File::escapeGitFilename(array));
                    } else {
                        QByteArray array(buf + 6, strlen(buf) - 7);
                        m_file.setOldFileName(File::escapeGitFilename(array));
                    }
                }
            }
            else if (m_fileToDiff == 0 && line.length() > 6 &&
                    (line.startsWith("+++ b/") || line.startsWith("+++ \"b"))) {
                if (m_state == InPatch)
                    storeFile();
                m_state = InHeader;
                if (line[4].unicode() == '"') { // git-encoding...
                    QByteArray array(buf + 7, strlen(buf) - 8);
                    array.prepend('"');
                    m_file.setFileName(File::escapeGitFilename(array));
                } else {
                    m_file.setFileName(QByteArray(buf + 6, strlen(buf) - 7));
                }
            }
            else if (line.length() > 5 && line.startsWith("@@ -")) {
                m_file.addHunk(m_hunk);
                m_hunk = Hunk();
                m_state = InPatch;
            }
            else if (line.startsWith("diff --git ")) {
                if (m_state == InPatch)
                    storeFile();
                m_state = InHeader;
            }
            else if (line.startsWith("Binary files ") && line.indexOf(" differ") > 0) {
                Q_ASSERT(m_fileToDiff);
                m_fileToDiff->setBinary(true);
                return true;
            }
            else if (line.startsWith("index ")) {
                if (m_state == InPatch)
                    storeFile();
                m_state = InHeader;
                int dot = line.indexOf(QLatin1Char('.'), 6);
                if (dot > 0 && line.length() > dot+3) {
                    m_file.setOldSha1(line.mid(6, dot-6));
                    int space = line.indexOf(QLatin1Char(' '), dot);
                    if (space == -1) {
                        space = line.length()-1; // cut off the linefeed
                    } else {
                        m_file.setProtection(line.mid(space+1).trimmed());
                    }
                    m_file.setSha1(line.mid(dot+2, space - dot - 2));
                }
            }
            else if (line.startsWith("deleted file mode ")) {
                if (m_state == InPatch)
                    storeFile();
                m_state = InHeader;
                m_file.setProtection(line.mid(18).trimmed());
            }
            else if (line.startsWith("new file mode ")) {
                if (m_state == InPatch)
                    storeFile();
                m_state = InHeader;
                m_file.setProtection(line.mid(13).trimmed());
            }
            return false;
        }

        QList<File> m_filesInDiff;
        QList<File> m_newFiles;
        QList<File> m_removedFiles;

        QIODevice &m_input;
        File *m_fileToDiff;
        State m_state;
        File m_file;
        Hunk m_hunk;
        char buf[10240];
    };

    States stateMachine(git, fileToDiff);
    stateMachine.start();
    return stateMachine.results();
}

void ChangeSet::addFile(const File &file)
{
    if (file.isValid())
        d->files << file;
}

int ChangeSet::count() const
{
    return d->files.count();
}

void ChangeSet::writeDiff(QIODevice &outDevice, ChangeSet::Selection selection) const
{
    waitFinishGenerateHunks();
    outDevice.open(QIODevice::WriteOnly | QIODevice::Truncate);
    QDataStream diff(&outDevice);
    foreach(File file, d->files) {
        if ((selection == AllHunks
                    || (selection == UserSelection && file.renameAcceptance() == Vng::Accepted))
                && !file.oldFileName().isEmpty() && !file.fileName().isEmpty()
                && file.oldFileName() != file.fileName()) {
            writeRenameDiff(diff, file);
            continue;
        }
        if (file.isBinary() && (selection == AllHunks || (selection == UserSelection
                        && file.binaryChangeAcceptance() == Vng::Accepted))) {
            diff.writeRawData("diff --git a/", 13);
            QByteArray fileName = file.oldFileName();
            if (fileName.isEmpty())
                fileName = file.fileName();
            diff.writeRawData(fileName.data(), fileName.size());
            diff.writeRawData(" b/", 3);
            fileName = file.fileName();
            if (fileName.isEmpty())
                fileName = file.oldFileName();
            diff.writeRawData(fileName.data(), fileName.size());
            if (file.oldFileName().isEmpty()) {
                diff.writeRawData("\nnew file mode ", 15);
                QByteArray protection = file.protection().toLatin1();
                diff.writeRawData(protection.data(), protection.size());
            } else if (file.fileName().isEmpty()) {
                diff.writeRawData("\ndeleted file mode ", 19);
                QByteArray protection = file.oldProtection().toLatin1();
                diff.writeRawData(protection.data(), protection.size());
            }
            diff.writeRawData("\nindex ", 7);
            QByteArray sha1 = file.oldSha1().toLatin1();
            diff.writeRawData(sha1.data(), sha1.size());
            diff.writeRawData("..", 2);
            sha1 = file.sha1().toLatin1();
            diff.writeRawData(sha1.data(), sha1.size());
            diff.writeRawData("\nGIT binary patch\n", 18);
            //file.writeBinaryDataAsPatch(outDevice); // TODO :)
            diff.writeRawData("literal 0\nHcmV?d00001\n\n", 23);
            continue;
        }

        bool fileHeaderWritten = false;
        foreach(Hunk hunk, file.hunks()) {
            if (selection == AllHunks
                || (selection == UserSelection
                  && (hunk.acceptance() == Vng::Accepted || hunk.acceptance() == Vng::MixedAcceptance))
                || (selection == InvertedUserSelection && hunk.acceptance() != Vng::Accepted)) {
                if (!fileHeaderWritten) {
                    if (file.oldFileName().isEmpty()) { // new file
                        diff.writeRawData("--- /dev/null", 13);
                    } else {
                        diff.writeRawData("--- a/", 6);
                        diff.writeRawData(file.oldFileName().data(), file.oldFileName().size());
                    }
                    if (file.fileName().isEmpty()) { // deleted file
                        diff.writeRawData("\n+++ /dev/null\n", 15);
                    } else {
                        diff.writeRawData("\n+++ b/", 7);
                        diff.writeRawData(file.fileName().data(), file.fileName().size());
                        diff.writeRawData("\n", 1);
                    }
                    fileHeaderWritten = true;
                }
                QByteArray acceptedPatch;
                if (selection == InvertedUserSelection)
                    acceptedPatch =hunk.rejectedPatch();
                else if (selection == AllHunks) {
                    acceptedPatch = hunk.header();
                    acceptedPatch.append(hunk.patch());
                }
                else
                    acceptedPatch = hunk.acceptedPatch();
                diff.writeRawData(acceptedPatch.data(), acceptedPatch.size());
            }
        }
    }
    outDevice.close();
}

void ChangeSet::writeRenameDiff(QDataStream &out, const File &file) const
{
    out.writeRawData("diff --git a/", 13);
    out.writeRawData(file.fileName().data(), file.fileName().size());
    out.writeRawData(" b/", 3);
    out.writeRawData(file.fileName().data(), file.fileName().size());
    out.writeRawData("\nnew file mode ", 15);
    QByteArray tmp = file.protection().toLatin1();
    out.writeRawData(tmp.data(), tmp.length());
    out.writeRawData("\nindex 0000000..", 16);
    tmp = file.sha1().toLatin1();
    out.writeRawData(tmp.data(), 7);
    out.writeRawData("\n--- /dev/null\n+++ b/", 21);
    out.writeRawData(file.fileName().data(), file.fileName().size());

    QProcess git;
    QStringList arguments;
    arguments << "cat-file" << "blob" << file.sha1();
    GitRunner runner(git, arguments);
    runner.start(GitRunner::WaitForStandardOutput);
    QList<QByteArray> lines;
    char buf[4096];
    while(true) {
        qint64 lineLength = Vng::readLine(&git, buf, sizeof(buf));
        if (lineLength == -1)
            break;
        lines.append(QByteArray(buf, lineLength));
    }

    QByteArray countAsString = QString::number(lines.count()).toLatin1();
    out.writeRawData("\n@@ -0,0 +1,", 12);
    out.writeRawData(countAsString.data(), countAsString.length());
    out.writeRawData(" @@\n", 4);
    foreach (const QByteArray &line, lines) {
        out.writeRawData("+", 1);
        out.writeRawData(line.data(), line.length());
    }

    // removed file.
    out.writeRawData("\ndiff --git a/", 14);
    out.writeRawData(file.oldFileName().data(), file.oldFileName().size());
    out.writeRawData(" b/", 3);
    out.writeRawData(file.oldFileName().data(), file.oldFileName().size());
    out.writeRawData("\ndeleted file mode ", 19);
    tmp = file.oldProtection().toLatin1();
    out.writeRawData(tmp.data(), tmp.length());
    out.writeRawData("\nindex ", 7);
    tmp = file.oldSha1().toLatin1();
    out.writeRawData(tmp.data(), 7);
    out.writeRawData("..0000000\n--- a/", 16);
    out.writeRawData(file.oldFileName().data(), file.oldFileName().size());

    out.writeRawData("\n+++ /dev/null\n@@ -1,", 21);
    out.writeRawData(countAsString.data(), countAsString.length());
    out.writeRawData(" +0,0 @@\n", 9);

    foreach (const QByteArray &line, lines) {
        out.writeRawData("-", 1);
        out.writeRawData(line.data(), line.length());
    }
}

bool ChangeSet::hasAcceptedChanges() const
{
    waitFinishGenerateHunks();
    foreach(File file, d->files) {
        if (file.renameAcceptance() == Vng::Accepted && file.fileName() != file.oldFileName())
            return true;
        if (file.protectionAcceptance() == Vng::Accepted && file.protection() != file.oldProtection())
            return true;
        if (file.isBinary() && file.binaryChangeAcceptance() == Vng::Accepted)
            return true;
        foreach(Hunk hunk, file.hunks()) {
            Vng::Acceptance a = hunk.acceptance();
            if (a == Vng::Accepted || a == Vng::MixedAcceptance)
                return true;
        }
    }
    return false;
}

File ChangeSet::file(int index) const
{
    // qDebug() << "ChangeSet::file" << index << d->finishedOneHunk;
    waitForFinishFirstFile();
    QMutexLocker ml2(&d->fileAccessLock);
    while (d->files.count() > index && d->files[index] == d->lockedFile)
// { qDebug() << "  waiting for file to be unlocked";
        d->fileAccessWaiter.wait(&d->fileAccessLock);
// }
    if (d->files.count() <= index)
        return File();

    // qDebug() << "ChangeSet::~file";
    return d->files[index];
}

ChangeSet &ChangeSet::operator=(const ChangeSet &other)
{
#if QT_VERSION >= 0x040400
    other.d->ref.ref();
    if (!d->ref.deref())
#else
    other.d->ref++;
    if (--d->ref == 0)
#endif
        delete d;
    d = other.d;
    return *this;
}

void ChangeSet::waitFinishGenerateHunks() const
{
    if (d->hunksFetcher)
        d->hunksFetcher->wait();
}

void ChangeSet::waitForFinishFirstFile() const
{
    d->cursorAccessLock.lock();
    if (! d->finishedOneHunk)
        d->cursorAccessWaiter.wait(&d->cursorAccessLock);
    d->cursorAccessLock.unlock();
}

