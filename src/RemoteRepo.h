#ifndef REMOTEREPO_H
#define REMOTEREPO_H

#include <QString>
#include <QExplicitlySharedDataPointer>

// TODO move to .cpp
class RemoteRepoPrivate : public QSharedData
{
public:
    RemoteRepoPrivate() : isDefaultRepo(false) { }
    QString name;
    QString url;
    bool isDefaultRepo;
};

/**
 * A repository that is registered in the configuration.
 */
class RemoteRepo
{
public:
    RemoteRepo();
    RemoteRepo(const RemoteRepo &other);
    RemoteRepo(const QString &name, const QString &url);
    ~RemoteRepo();

    void setIsDefault(bool on);
    bool isDefault() const;

    QString name() const;
    QString url() const;

    bool isValid() const { return d; }

private:
    QExplicitlySharedDataPointer<RemoteRepoPrivate> d;
};

#endif
