#include "TrackedBranch.h"

TrackedBranch::TrackedBranch()
{
}

TrackedBranch::TrackedBranch(const TrackedBranch &other)
    : d(other.d)
{
}

TrackedBranch::TrackedBranch(const QString &localName, const QString &remoteName, const RemoteRepo &remote)
    : d(new TrackedBranchPrivate())
{
    d->remoteBranch = remoteName;
    d->localBranch = localName;
    d->remote = remote;
}

TrackedBranch::~TrackedBranch()
{
}

QString TrackedBranch::localName() const
{
    return d->localBranch;
}

QString TrackedBranch::remoteName() const
{
    return d->remoteBranch;
}

RemoteRepo TrackedBranch::remote() const
{
    return d->remote;
}

