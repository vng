/*
 * This file is part of the vng project
 * Copyright (C) 2008 Thomas Zander <tzander@trolltech.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENERICCURSOR_H
#define GENERICCURSOR_H

#include "InterviewCursor.h"
#include "Vng.h"

/// A cursor implementation that can work on a pre-added dataset.
class GenericCursor : public InterviewCursor
{
public:
    /// Selects quit-behavion
    enum AcceptanceMode {
        ExitOnAccept,
        ExitWhenDone
    };

    GenericCursor(AcceptanceMode mode = ExitOnAccept);
    virtual ~GenericCursor();

    /// returns the new index
    virtual int forward(Scope scope = ItemScope, bool skipAnswered = true);
    /// returns the new index
    virtual int back(Scope scope = ItemScope);
    virtual void setResponse(bool response, Scope scope = ItemScope);

    void setAllowedOptions(QString &options);
    virtual QString allowedOptions() const;

    virtual int count();
    virtual void forceCount();

    virtual QString currentText() const;
    void setHelpMessage(const QString &message);
    virtual QString helpMessage() const;
    virtual bool isValid() const;

    int addDataItem(const QString &text);

    QList<int> selectedItems() const;

private:
    class Item {
      public:
        Item(const QString &text);
        QString text() const;

        Vng::Acceptance m_acceptance;

      private:
        QString m_text;
    };
    QList<Item*> m_items;
    int m_curentIndex;

    QString m_allowedOptions;
    QString m_helpMessage;
    AcceptanceMode m_mode;
    bool m_oneAccepted;
};

#endif
