#ifndef TRACKEDBRANCH_H
#define TRACKEDBRANCH_H

#include <QString>
#include <QExplicitlySharedDataPointer>

#include "RemoteRepo.h"

class TrackedBranchPrivate;
class RemoteRepo;

// TODO move to .cpp file
class TrackedBranchPrivate : public QSharedData
{
public:
    QString remoteBranch;
    QString localBranch;
    RemoteRepo remote;
};

/**
 * A local branch that is registered in the configuration to track a remote branch.
 */
class TrackedBranch
{
public:
    TrackedBranch();
    TrackedBranch(const TrackedBranch &trackedBranch);
    TrackedBranch(const QString &localName, const QString &remoteName, const RemoteRepo &remote);
    ~TrackedBranch();

    QString localName() const;
    QString remoteName() const;
    RemoteRepo remote() const;

    bool isValid() const { return d; }

private:
    QExplicitlySharedDataPointer<TrackedBranchPrivate> d;
};

#endif
