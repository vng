/*
 * This file is part of the vng project
 * Copyright (C) 2008 Thomas Zander <tzander@trolltech.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GenericCursor.h"

#include <QDebug>

GenericCursor::GenericCursor(AcceptanceMode mode)
    : InterviewCursor(),
    m_curentIndex(0),
    m_mode(mode),
    m_oneAccepted(false)
{
    switch (m_mode) {
    case ExitOnAccept:
        m_allowedOptions = QLatin1String("ynqjk");
        break;
    case ExitWhenDone:
        m_allowedOptions = QLatin1String("ynqadjk");
        break;
    }
}

GenericCursor::~GenericCursor()
{
    qDeleteAll(m_items);
}

int GenericCursor::forward(Scope scope, bool skipAnswered)
{
    Q_UNUSED(scope);
    do {
        if (m_items.count() <= m_curentIndex + 1) {
            if (m_mode == ExitWhenDone)
                ++m_curentIndex;
            return m_items.count();
        }
        ++m_curentIndex;
        if (m_items[m_curentIndex]->m_acceptance == Vng::Undecided)
            break;
    } while (skipAnswered);
    return m_curentIndex+1;
}

int GenericCursor::back(Scope scope)
{
    Q_UNUSED(scope);
    while (m_curentIndex > 0) {
        m_curentIndex--;
        if (m_items[m_curentIndex]->m_acceptance == Vng::Undecided)
            break;
    }
    return m_curentIndex+1;
}

void GenericCursor::setResponse(bool response, Scope scope)
{
    Q_UNUSED(scope);
    if (m_curentIndex >= 0 && m_curentIndex < m_items.count()) {
        m_items[m_curentIndex]->m_acceptance = response ? Vng::Accepted : Vng::Rejected;
        if (response)
            m_oneAccepted = true;
    }
}

void GenericCursor::setAllowedOptions(QString &options)
{
    m_allowedOptions = options;
}

QString GenericCursor::allowedOptions() const
{
    return m_allowedOptions;
}

int GenericCursor::count()
{
    return m_items.count();
}

void GenericCursor::forceCount()
{
}

QString GenericCursor::currentText() const
{
    return m_items[m_curentIndex]->text();
}

void GenericCursor::setHelpMessage(const QString &message)
{
    m_helpMessage = message;
}

QString GenericCursor::helpMessage() const
{
    return m_helpMessage;
}

bool GenericCursor::isValid() const
{
    if (m_mode == ExitOnAccept)
        return !m_oneAccepted;
    return m_curentIndex >= 0 && m_curentIndex < m_items.count();
}

int GenericCursor::addDataItem(const QString &text)
{
    Item *item = new Item(text);
    m_items << item;
    return m_items.count();
}

QList<int> GenericCursor::selectedItems() const
{
    int index = 0;
    QList<int> answer;
    foreach (Item *item, m_items) {
        if (item->m_acceptance == Vng::Accepted)
            answer << index;
        ++index;
    }
    return answer;
}


GenericCursor::Item::Item(const QString &text)
    : m_acceptance(Vng::Undecided),
    m_text(text)
{
}

QString GenericCursor::Item::text() const
{
    return m_text;
}
