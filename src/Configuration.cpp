/*
 * This file is part of the vng project
 * Copyright (C) 2008-2009 Thomas Zander <tzander@trolltech.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Configuration.h"
#include "Logger.h"
#include "GitRunner.h"
#include "AbstractCommand.h"

#include <QDebug>
#include <QMutexLocker>
#include <QProcess>

#ifndef Q_OS_WIN
#include <unistd.h>
#endif

Configuration::Configuration(const char *section)
    : m_repoDir(QLatin1String(".")),
    m_section(QString::fromLatin1(section)),
    m_vngConfigRead(false),
    m_emptyRepo(false),
    m_fetchedBranches(false),
    m_configRead(false)
{
}

bool Configuration::contains(const QString & key) const
{
    const_cast<Configuration*> (this)->readVngConfig();
    return m_options.contains(key);
}

QDir Configuration::repository() const
{
    const_cast<Configuration*> (this)->readVngConfig();
    return m_repoDir;
}

QDir Configuration::repositoryMetaDir() const
{
    const_cast<Configuration*> (this)->readVngConfig();
    return m_repoMetaDataDir;
}

void Configuration::readVngConfig()
{
    if (m_vngConfigRead)
        return;
    m_vngConfigRead = true;
    QObject deleterParent;

    QDir dir = QDir::current();
    do {
        QDir git = dir;
        if (git.cd(QLatin1String(".git"))) {
            m_repoDir = dir;
            m_repoMetaDataDir = git;
            QDir refs(git.absoluteFilePath(QLatin1String("refs/heads")));
            m_emptyRepo = refs.count() == 2; // only '.' and '..'
            if (m_emptyRepo) {
                QFile refs(git.absolutePath() + QLatin1String("/packed-refs"));
                m_emptyRepo = !refs.exists();
            }
            break;
        }
        if (!dir.cdUp())
            break;
    } while(!dir.isRoot());

    QString home = QDir::homePath();
    QFile *config;
    config = new QFile(home + QLatin1String("/.vng/config"), &deleterParent);
    if (! config->exists())
        config = new QFile(home + QLatin1String("/.darcs/defaults"), &deleterParent);
    if (config->exists()) {
        if (! config->open(QIODevice::ReadOnly)) {
            Logger::error() << "Failed to open config file, is it readable?\n";
            return;
        }

        char buf[1024];
        while(true) {
            qint64 lineLength = config->readLine(buf, sizeof(buf));
            if (lineLength == -1)
                break;
            QString line = QString::fromUtf8(buf, lineLength);
            QString option;
            if (line.startsWith(QLatin1String("ALL ")))
                option = line.mid(3).trimmed();
            else if (line.length() > m_section.length() && line.startsWith(m_section))
                option = line.mid(m_section.length()).trimmed();
            if (! option.isEmpty()) {
                const int index = option.indexOf(QLatin1Char(' '));
                if (index > 0)
                    m_options.insert(option.left(index).trimmed(), option.mid(index).trimmed());
                else
                    m_options.insert(option, QString());
            }
        }
        config->close();
    }
}

void Configuration::readConfig()
{
    if (m_configRead)
        return;
    m_configRead = true;

    readVngConfig();
    QFile config(m_repoMetaDataDir.absolutePath() + QLatin1String("/config"));
    if (! config.open(QIODevice::ReadOnly))
        return;

    class RepoCreator {
      public:
        RepoCreator() : active(false) { }
        QString name;
        QString repoUrl;
        bool active;

        void create(QList<RemoteRepo> &repos) {
            if (active && !repoUrl.isEmpty())
                repos.append(RemoteRepo(name, repoUrl));
            active = false;
            name.clear();
            repoUrl.clear();
        }
    };
    RepoCreator repoCreator;
    class BranchCreator {
      public:
        BranchCreator() : active(false) { }
        QString localName;
        QString remoteName;
        QString repoName;
        bool active;

        void create(QList<TrackedBranch> &branches, const QList<RemoteRepo> &repos) {
            if (active && !localName.isEmpty() && !repoName.isEmpty()) {
                foreach (RemoteRepo repo, repos) {
                    if (repo.name() == repoName) {
                        branches.append(TrackedBranch(localName, remoteName, repo));
                    }
                }
            }
            active = false;
            localName.clear();
            remoteName.clear();
            repoName.clear();
        }
    };
    BranchCreator branchCreator;

    char bytes[1024];
    QString defaultRepo;
    bool vngRemotes = false;
    while (true) {
        qint64 lineLength = Vng::readLine(&config, bytes, 1024);
        if (lineLength == -1)
            break;
        char *line = bytes;
        while (lineLength > 0 && (line[0] == ' ' || line[0] == '\t')) {
            ++line;
            --lineLength;
        }
        if (lineLength == 0 || line[0] == '#' || line[0] == ';')
            continue;

        if (line[0] == '[') { // start section
            repoCreator.create(m_remoteRepos);
            branchCreator.create(m_trackedBranches, m_remoteRepos);
            vngRemotes = false;
            QString sectionLine = QString::fromAscii(line+1, lineLength-1).trimmed();
            if (sectionLine.startsWith(QLatin1String("remote \""))) {
                repoCreator.active = true;
                repoCreator.name = sectionLine.mid(8, sectionLine.length()-10);
            } else if (sectionLine.startsWith(QLatin1String("branch \""))) {
                branchCreator.active = true;
                branchCreator.remoteName = sectionLine.mid(8, sectionLine.length()-10);
            } else if (sectionLine.startsWith(QLatin1String("vng \"remote\""))) {
                vngRemotes = true;
            }
        }
        else if (vngRemotes || repoCreator.active || branchCreator.active) {
            QString variableLine = QString::fromAscii(line, lineLength-1);
            int index = variableLine.indexOf(QLatin1Char('='));
            if (index <= 0)
                continue;
            QString variable = variableLine.left(index).trimmed();
            QString value = variableLine.mid(index+1).trimmed();
            if (variable == QLatin1String("url") && repoCreator.active) {
                repoCreator.repoUrl = value;
            } else if (branchCreator.active && variable == QLatin1String("remote")) {
                branchCreator.repoName = value;
            } else if (branchCreator.active && variable == QLatin1String("merge")) {
                index = value.lastIndexOf(QLatin1Char('/'));
                if (index > 0 && value.length() > index)
                    branchCreator.localName = value.mid(index+1);
            } else if (vngRemotes && variable == QLatin1String("default")) {
                defaultRepo = value;
            } else if (vngRemotes && variable == QLatin1String("url")) {
                bool found = false;
                foreach (const RemoteRepo &repo, m_remoteRepos) {
                    if (repo.url() == value) { // no need to add it twice
                        found = true;
                        break;
                    }
                }
                if (!found)
                    m_remoteRepos.append(RemoteRepo(value, value));
            }
        }
    }
    repoCreator.create(m_remoteRepos);
    branchCreator.create(m_trackedBranches, m_remoteRepos);

    foreach (RemoteRepo repo, m_remoteRepos) {
        if (repo.url() == defaultRepo) {
            repo.setIsDefault(true);
            break;
        }
    }
}

QString Configuration::optionArgument(const QString &optionName, const QString &defaultValue) const
{
    if (m_options.contains(optionName))
        return m_options[optionName];
    return defaultValue;
}

bool Configuration::colorTerm() const
{
#ifndef Q_OS_WIN
    if (isatty(1))
        return QString::fromLatin1(getenv("TERM")) != QLatin1String("dumb") && !Logger::hasNonColorPager();
#endif
    return false;
}

bool Configuration::isEmptyRepo() const
{
    const_cast<Configuration*> (this)->readVngConfig();
    return m_emptyRepo;
}

QList<Branch> Configuration::allBranches()
{
    fetchBranches();
    QList<Branch> answer;
    answer += m_localBranches;
    answer += m_remoteBranches;
    return answer;
}

QList<Branch> Configuration::branches()
{
    fetchBranches();
    return m_localBranches;
}

QList<Branch> Configuration::remoteBranches()
{
    fetchBranches();
    return m_remoteBranches;
}

void Configuration::fetchBranches()
{
    if (m_fetchedBranches)
        return;
    m_fetchedBranches = true;

    QProcess git;
    QStringList arguments;
    arguments << QLatin1String("ls-remote") << QLatin1String(".");
    GitRunner runner(git, arguments);
    AbstractCommand::ReturnCodes rc = runner.start(GitRunner::WaitForStandardOutput);
    if (rc != AbstractCommand::Ok) {
        return;
    }
    char buf[1024];
    while(true) {
        qint64 lineLength = Vng::readLine(&git, buf, sizeof(buf));
        if (lineLength == -1)
            break;
        if (lineLength > 46) { // only take stuff that is in the 'refs' dir.
            QString name = QString::fromUtf8(buf + 46);
            const bool remotes = name.startsWith(QLatin1String("remotes/"));
            if (!remotes && !name.startsWith(QLatin1String("heads/")))
                continue;

            name = name.trimmed(); // remove linefeed
            Branch branch(name, QString::fromLatin1(buf, 40));
            if (name.startsWith(QLatin1String("remotes")))
                m_remoteBranches.append(branch);
            else
                m_localBranches.append(branch);
        }
    }

    QFile localRef(repositoryMetaDir().absolutePath()+ QLatin1String("/HEAD"));
    if (localRef.open(QIODevice::ReadOnly)) {
        qint64 lineLength = Vng::readLine(&localRef, buf, sizeof(buf));
        if (lineLength >= 1) {
            QString line = QString::fromLatin1(buf);
            if (line.startsWith(QLatin1String("ref: refs/"))) {
                QString head = line.mid(10, line.length()-11);
                foreach (Branch branch, m_localBranches) {
                    if (branch.branchName() == head) {
                        branch.setIsHead(true);
                        break;
                    }
                }
            }
        }
    }
}

void Configuration::pullConfigDataFrom(const Configuration &other)
{
    if (other.m_fetchedBranches) {
        m_localBranches = other.m_localBranches;
        m_remoteBranches = other.m_remoteBranches;
        m_fetchedBranches = true;
    }
    if (other.m_configRead) {
        m_remoteRepos = other.m_remoteRepos;
        m_trackedBranches = other.m_trackedBranches;
        m_configRead = true;
    }
}

QList<TrackedBranch> Configuration::trackedBranches()
{
    const_cast<Configuration*> (this)->readConfig();
    return m_trackedBranches;
}

QList<RemoteRepo> Configuration::remotes()
{
    const_cast<Configuration*> (this)->readConfig();
    return m_remoteRepos;
}

void Configuration::addRepo(const RemoteRepo &newRepo, AddStrategy strategy)
{
    RemoteRepo defaultRepo;
    // check if its there already.
    foreach (const RemoteRepo &repo, m_remoteRepos) {
        if (repo.url() == newRepo.url()) {
            if (strategy == AddNotDefault)
                return;
            if (repo.isDefault())
                return;
        } else if (repo.isDefault()) {
            defaultRepo = repo;
        }
    }
    QProcess git;
    QStringList arguments;
    GitRunner runner(git, arguments);
    if (defaultRepo.isValid() && strategy == AddAsDefault) {
        // move the default one to a normal url.
        arguments << QLatin1String("config") << QLatin1String("--add")
            << QLatin1String("vng.remote.url") << defaultRepo.url();
        runner.setArguments(arguments);
        AbstractCommand::ReturnCodes rc = runner.start(GitRunner::WaitUntilFinished);
        if (rc != AbstractCommand::Ok) {
            Logger::warn() << "Failed to store remote repository in local config; check permissions\n";
            return;
        }
    }
    // store it as default.
    arguments.clear();
    arguments << QLatin1String("config");
    if (strategy == AddAsDefault)
        arguments  << QLatin1String("--replace-all") << QLatin1String("vng.remote.default");
    else
        arguments << QLatin1String("--add") << QLatin1String("vng.remote.url");
    arguments << newRepo.url();
    runner.setArguments(arguments);
    AbstractCommand::ReturnCodes rc = runner.start(GitRunner::WaitUntilFinished);
    if (rc != AbstractCommand::Ok) {
        Logger::warn() << "Failed to store remote repository in local config; check permissions\n";
        return;
    }
}

